<?php

declare(strict_types=1);

namespace Drupal\cas\Exception;

/**
 * Extends \Exception.
 */
class CasSloException extends \Exception {
}
